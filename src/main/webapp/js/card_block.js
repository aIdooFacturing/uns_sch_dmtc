//machineList id, company, name, comIp, x, y
var cPage = 1;
var panel = false;
var colors, colors_a;
var subMenuTr = false;
var status3, status4;
var circleWidth = contentWidth * 0.07;
var canvas;
var ctx;
var dvcName = [];
var csvData = "설비명, 날짜, 가동형번, 사이클타임, 표준생산수량, 현 생산수량, 가동시간, 비가동시간, 알람발생횟수, 최종정지시간, 가동률 LINE";

var admin = false;
$(function() {
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	today = year + "-" + month + "-" + day;
	
	$("#time_table").click(function(){
		location.href = ctxPath + "/chart/chart_rail.do";
	});
	
	BindEvt();
	$("#main_logo").click(function(){
		if(!admin){
			enterAdminMode();
		}else{
			exitAdminMode();
			admin = false;
		};
	});
	
	getToday();
	$("#excel").click(csvSend);
	canvas = document.getElementById("canvas");
	canvas.width = contentWidth;
	canvas.height = contentHeight;

	ctx = canvas.getContext("2d");
	
	//$("#comName").html(window.sessionStorage.getItem("company"));
	setEl();
	//drawLine();
	setToday();
	getAllMachine();
	
	//drawLineChart("lineChart");
	
	evtBind();
	drawStatusPie("status_pie");
	drawStatusPie2("status_pie2");
	$("#panel_table tr:nth(1) td:nth(0) img").css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	//getDvcTemper(26);
	//getReportBarData();
	
	//$("#sDate, #eDate").change(getReportBarData);
	drawChart();
	getLeftChartData();
	getReportData();
	getMonthlyBlockTatget();
	getTargetData();
});

var targetMap = new JqMap();
var targetArray = new Array();
function getTargetData(){
	var url = ctxPath + "/chart/getTargetBlockData.do";
	var param = "tgDate=" + today + 
				"&type=block";
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var  json = data.dataList;
			
			var tr = "<tr>" + 
						"<td>장비</td><td style='text-align: center;'>목표 사이클</td>" + 
					"</tr>";
			var class_name = "";
			console.log(json)
			$(json).each(function(idx, data){
				if(data.isEmpty==1){
					class_name = "disable"
				}else{
					class_name = " "
					targetMap.put("t" + data.dvcId, data.tgCnt);
				}
				
				var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
				tr += "<tr>" + 
							"<td>" + name + "</td>" +
							"<td> <input type='test' id=t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
					  "</tr>";
				
			});
			
			
			tr += "<tr>" + 
						"<td colspan='2' style='text-align:center'><span style='cursor : pointer' id='save_btn'>저장</span></td>" + 
					"</tr>";
			
			$("#machineListTable").html(tr);
			$("#save_btn").click(addTarget);
			
			$(".targetCnt").css({
				"font-size" : getElSize(40),
				"outline" : "none",
				"border" : "none"
			});
			
			$(".span").css({
				"font-size" : getElSize(40),
				"color" : "red",
				"margin-left" : getElSize(10),
			});
			
			$("#save_btn").css({
				"background-color" : "white",
				"color" : "black",
				"border-radius" : getElSize(10),
				"font-weight" : "bolder",
				"padding" : getElSize(10)
			});
			
			$(".tdisable").each(function(idx, data){
				this.disabled = true;
				this.value = "";
			});
			
			$(".tdisable").css({
				"background-color" : "rgba(	4,	238,	91,0.5)"
			});
			
			$("#machineListTable td").css({
				"color" : "white",
				"font-size" : getElSize(50),
			});
			
			$("#machineListForTarget").css({
				"width" : getElSize(800)
			});
			
			$("#machineListForTarget").css({
				"position" : "absolute",
				"left" : (originWidth/2) - ($("#machineListForTarget").width()/2),
				"top" : getElSize(50),
				//"background-color" : "rgb(34,34,34)",
				"background-color" : "green",
				"color" : "white",
				"font-size" : getElSize(50),
				"padding" : getElSize(50),
				"overflow" : "auto",
				"border-radius" : getElSize(50),
				"border" : getElSize(10) + "px solid white",
			});
		}
	});
	
};

function showMonthlyTargetBox(){
	$("#monthlyTargetBox").fadeIn(500);
};


var tgArray = new Array();
var target_i = 0;
function addTarget(){
	tgArray = targetMap.keys();
	var url = ctxPath + "/chart/addBlockTargetCnt.do";
	
	var cnt = $("#" + tgArray[target_i]).val();
	if(cnt==null || cnt == "") cnt = 0;
	 
	var param = "dvcId=" + tgArray[target_i].substr(1) + 
				"&tgCnt=" + cnt +  
				"&tgDate=" + today;

	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			if(data=="success") {
				if (target_i<(tgArray.length-1)){
					target_i++;
					console.log(target_i, tgArray.length)
					addTarget(target_i);
				}else{
					target_i = 0;
					$("#machineListForTarget, #close_btn").animate({
						"opacity" :0
					}, function(){
						$("#machineListForTarget").css("z-index",0);
						$("#close_btn").css("z-index",0	);
						getTargetData();
					});
				}
				
			}else{
				//i
			}
		}
	});
	
	
};

function getMonthlyBlockTatget(){
	var url = ctxPath + "/chart/getMonthlyBlockTatget.do";
	
	$.ajax({
		url : url,
		dataType: "text",
		type : "post",
		success :function(data){
			$("#monthlyTarget").html("월 목표량 : " + data + "EA");
			$("#monthlyTargetValue").val(data);
			
			$("#edit_monthly_target").css({
				"width" : getElSize(80),
				"position" : "absolute",
				"cursor" : "pointer",
				"top" : $("#monthlyTarget").offset().top,
				"left" : $("#monthlyTarget").offset().left - getElSize(100),
				"z-index" :"9"
			});
		}
	});
};

var rowSpanArray = new Array();
var sum = 0;
function getReportData(){
	var url = ctxPath + "/chart/getReportData.do";
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	var type = "block";
	var param = "type=" + type + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate;
	
	rowSpanArray = new Array();
	csvData = "설비, 날짜, 형번, 가동시간, 비가동시간,  가동률(%), 사이클타임, 표준생산수량, 완료 생산수량,  알람 횟수, 최종정지시간 LINE";
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var preName = json[0].name;
			var i = 0;
			
			$(json).each(function(idx, data){
				if(preName!=data.name){
					preName = data.name;
					rowSpanArray.push(i);
					i = 0;
				};
				i++;
			});
			
			sum = 0;
			for(var i = 0; i < rowSpanArray.length; i++){
				sum += rowSpanArray[i];
			};
			
			rowSpanArray.push(json.length - sum);
			
			var tr = "<thead >"+							
						"<Tr align='center' style='color:white' class='thead'>"+
							"<th>"+
								"설비"+
							"</th>"+
							"<th>"+
								"날짜" +
							"</th>"+
							"<th>"+
								"형번"+
							"</th>"+
							"<th>"+
								"가동<br>시간"+
							"</th>"+
							"<th>"+
								"비가동<br>시간"+
							"</th>"+
							"<th>"+
								"가동률<br>(%)"+
							"</th>"+
							"<th>"+
								"사이클<Br>타임(분)"+
							"</th>"+
							"<th>"+
								"표준<br>생산수량"+
							"</th>"+
							"<th>"+
								"완료<br>생산수량"+
							"</th>"+
							"<th>"+
								"알람<br>횟수"+
							"</th>"+
							"<th>"+
								"최종<br>정지시간"+
							"</th>"+
						"</Tr>"+
					"</thead>" + 
					"<div class='tbody'>";
			
			var preMachine = "";
			i = 0;
			var j = 0;
			var rowSpan = 1;
			
			var op_time = 0,
			off_time = 0,
			op_ratio = 0,
			cycle_time = 0,
			target_time = 0,
			compl_cycle = 0,
			total_alarm = 0;
			
			$(json).each(function(idx, data){
				var bgColor = "";
				if(idx%2==0){
					bgColor = "black";
				}else{
					bgColor = "#454545";
				};
				
				var name = "";
				if(preMachine!=data.name){
					rowSpan = rowSpanArray[i];
					//if(rowSpan>1) rowSpan+=1;
					
					preMachine = data.name;
					name = "<td align='center' width='10%' rowspan=" + rowSpan + " style='background-color: rgb(34,34,34)'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>";
					i++;
				}else{
					name = "";
				};
				
				
				tr += "<Tr>"+
							name + 
							"<td align='center' style='background-color:" + bgColor + "' width='10%'>" + data.workDate  + "</td>"+
							"<td align='center' style='background-color:" + bgColor + "' width='10%'>" + data.hyung  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='8%'>" + Math.round(data.crtRunningTimeSec / 3600) + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "'width='8%'>" + Math.round(data.downTimeSec / 3600)  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='8%'>" + Number(data.runningRatio*100).toFixed(1)  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='10%'>" + Number(data.avrCycleTimeSec/60).toFixed(1)  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='10%'>" + data.tgPrdctCnt  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='10%'>" + data.cycleCnt * 2  + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "' width='8%'>" + data.alarmCnt + "</td>"+
							"<td align='right' style='background-color:" + bgColor + "'> " + data.lastDownTime.substr(5) + "</td>"+
							
						"</Tr>";
				
				//요약 
				op_time += Math.round(data.crtRunningTimeSec / 3600);
				off_time +=  Math.round(data.downTimeSec / 3600)
				op_ratio += Number(Number(data.runningRatio*100).toFixed(1));
				cycle_time += Number(Number(data.avrCycleTimeSec/60).toFixed(1));
				target_time += Number(data.tgPrdctCnt);
				compl_cycle += data.cycleCnt * 2;
				total_alarm += 0;
					
				j++;

				if((rowSpan-1) == j && rowSpan > 1){
					
//					tr += "<Tr >"+
//						"<td align='center' class='summary'>소계</td>"+
//						"<td align='right' class='summary'></td>"+
//						"<td align='right' class='summary'>"+ op_time + "</td>"+
//						"<td align='right' class='summary'>" + off_time + "</td>"+
//						"<td align='right' class='summary'>" + Number(op_ratio/j).toFixed(1) + "</td>"+
//						"<td align='right' class='summary'>" + cycle_time + "</td>"+
//						"<td align='right' class='summary'>" + target_time + "</td>"+
//						"<td align='right' class='summary'>" + compl_cycle + "</td>"+
//						"<td align='right' class='summary'>" + total_alarm + "</td>"+
//						"<td align='right' class='summary'> "  + "</td>"+
//					"</Tr>";
						
					j = 0;
					
					op_time = 0,
					off_time = 0,
					op_ratio = 0,
					cycle_time = 0,
					target_time = 0,
					compl_cycle = 0,
					total_alarm = 0;
				};
				
				if(rowSpan==1) {
					j = 0;
					size = 0,
					op_time = 0,
					off_time = 0,
					op_ratio = 0,
					cycle_time = 0,
					target_time = 0,
					compl_cycle = 0,
					total_size = 0;
					total_alarm = 0;
				};
				
				csvData+=   
							decodeURIComponent(data.name).replace(/\+/gi, " ") + "," +
							data.workDate + "," +
							data.hyung + ", " +
							Math.round(data.crtRunningTimeSec / 3600) + ", " + 
							Math.round(data.downTimeSec / 3600) + ", " +
							Number(data.runningRatio*100).toFixed(1)  + "," + 
							data.avrCycleTimeSec  + ", " + 
							data.tgPrdctCnt + ", " + 
							data.cycleCnt * 2 + ", " + 
							data.alarmCnt + "," + 
							data.lastDownTime.substr(5) + "LINE";
			});
			
			$("#table_wrapper").css({
				"overflow" : "auto",
				"height" : getElSize(1700)
			});
			
			tr+="</div>";
			$("#tableDiv").html(tr);
			
			$(".thead").css({
				"background-color" :"black",
				"position" : "fixed",
				"top" : $("#table_wrapper").offset().top
			});
			
			$(".thead").css({
				"background-color" :"black",
				"position" : "fixed",
				"top" : $("#table_wrapper").offset().top
			});
			
			$("#tableDiv td, #tableDiv th").css({
				"padding" : getElSize(10),
				"font-size" : getElSize(35),
		//		"background-color" : "rgb(34,34,34)",
				"color" : "white"
			});
			
			$(".summary").css({
				"background-color" : "rgba(255,255,255,0)",
				"color" : "white" , 
				"font-weight" : "bolder"
			});
			
			$("#tableDiv").css({
				"margin-top" : $(".thead").height()
			});

			$(".thead th").each(function(idx, data){
				$(data).css({
					"width" : $("#tableDiv tr:nth(1) td:nth(" + idx + ")").width()+1
				});
			});
		}
	});
};

function chkStatus(id){
	var result = false;
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("id")==id){
			var status = card_bar_data_array[i].get("status");
			if(status=="yellow") result = true;
		}
	};
	
	return result;
};

var targetCnt = 0;
var cCnt = 0;
var preRatio = 0;
function getLeftChartData(){
	var url = ctxPath + "/chart/getLeftBlockChart.do";
	
	var tr = "<tr >"+
				"<td>설비</td>"+
				"<td>형번</td>"+
				"<td>일일<br>목표<Br>수량</td>"+
				"<td>현재<br>생산<br>수량</td>"+
				//"<td>현재<Br>가동<br>시간(분)</td>"+
				"<td>대기<Br>시간(분)</td>"+
				//"<td>대기<Br>상황</td>"+
			"</tr>";
				
	targetCnt = 0;
	cCnt = 0;
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.chartList;
			
			var class_name = "e";
			$(json).each(function(idx, data){
				if(data.isEmpty==1){
					class_name = "disable"
				}else{
					class_name = " "
				}
				
				tr+= "<tr class='" + class_name + "'>" + 
						"<td>" + decodeURIComponent(data.name).substr(decodeURIComponent(data.name).indexOf("+")+1) + "</td>" +
						"<td>" + data.hyung + "</td>" + 
						"<td>" + data.tgCnt + "</td>" + 
						"<td>" + data.fnCnt*2 + "</td>" +
					//	"<td>" + "</td>" +
						"<td>" + Math.round(data.waitingTime/60) + "</td>" +
						//"<td>" + "</td>" +
					"</tr>";
				
				//대기시간 30분 이상
				clearAnim("circle" + data.dvcId);
				
				if(Math.round(data.waitingTime/60)>30 && chkStatus(data.dvcId) == true){
					animIcon("circle" + data.dvcId,1000);
				};
				
				targetCnt += data.tgCnt;
				cCnt += (data.fnCnt*2);
			});

			var n = cCnt/targetCnt*100;
			var ratio = Number(Number(n).toFixed(1))
			
			barChart.series[0].data[0].update(100);
//			if(preRatio!=ratio) {
//				barChart.series[0].data[0].update(ratio);
//				preRatio=ratio;
//			}
			
			$("#main_table").html(tr);
			
			$("#close_btn").css({
				"position" : "absolute",
				//"display" : "none",
				"width" : getElSize(100),
				"cursor" : "pointer",
				"left" : $("#machineListForTarget").offset().left + $("#machineListForTarget").width() +getElSize(50),
				"top" : $("#machineListForTarget").offset().top - getElSize(50),
		 	});
			
			$("#main_table td").css({
				"color" : "white",
				"border" : "solid 1px gray",
				"padding" : getElSize(5),
				"font-size" : getElSize(35)
			});
			
			$(".enable").css({
				"color" : "white"
			});
			
			$(".disable td").css({
				"background-color": "rgba(102,102,102,0.5)",
				"color" : "#666666"
			});
			
			$("#stopListTable").css({
				"left" : $("#main_table").offset().left,
				 "top" : $("#main_table").offset().top + $("#main_table").height() + getElSize(50)  
			});
			
			//edit btn
			$("#edit_daily_target").css({
				"width" : getElSize(80),
				"position" : "absolute",
				"cursor" : "pointer",
				"top" : $("#main_table").offset().top - getElSize(80),
				"left" : $("#main_table").offset().left,
				"z-index" :"9"
				//"background-color" : "white",
				//"border-radius" : "50%"
			});
		}
	});
	
	setTimeout(getLeftChartData, 5000);
};


var barChart;
var colors;
function drawChart(){
	var perShapeGradient = {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 1
        };
        colors = Highcharts.getOptions().colors;
        colors = [{
            linearGradient: perShapeGradient,
            stops: [
                    [0, '#6EF8FC'],
                    [1, '#BBFAF4']
                 ]
            }];
        
	$('#barChart').highcharts({
        chart: {
            type: 'bar',
            backgroundColor : "rgba(0,0,0,0)",
            width :	getElSize(3700),
            height : getElSize(150),
            marginTop :0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        exporting : false,
        credits :false,
        title: {
            text: false,
        },
        xAxis: {
            categories: [''],
            labels : {
        		enabled : false
        	},
        },
        yAxis: {
            min: 0,
            max : 100,
            title: {
                text: false
            },
            gridLineWidth: 0,
        },
        legend: {
            reversed: true,
        		enabled : false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                pointWidth : getElSize(200),
                dataLabels: {
                	format: '일일 목표 달성률 : {point.y:.1f}%',
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black',
                        fontSize : getElSize(100)
                    }
                }
            }
        },
        series: [{
            name: 'John',
            data: [0],
            borderWidth: 0,
            color : colors[0],
            shadow: {
                color: '#6EF8FC',
                width: getElSize(100),
                offsetX: 0,
                offsetY: 0
            }
        }]
    });
	
	barChart = $('#barChart').highcharts();
};

var isAdminMode = false;
function BindEvt(){
	$("#Rail").click(function(){
		location.href = ctxPath + "/chart/chart_rail.do";
	});
	
	$("#history").click(function(){
		location.href = ctxPath + "/chart/timeLine.do";
	});
	
	$("#pwd").bind("keyup", function(e){
		if(e.keyCode==13){
			var pwd = $("#pwd").val();
			if(pwd=="1234"){
				admin = true;
				hideCorver();
				enableDrawIcon();
				$("#admin_pwd_box").fadeOut(500);
				$(".edit").css("display","inline");
			}else{
				alert("비밀번호가 일치하지 않습니다.");
			}
		}
	});
	
	$("#corver").click(function(){
		hideCorver();
		$("#admin_pwd_box").fadeOut(500);
	});
	
	$("#monthly_target_save_btn").click(addMonthlyBlockTarget);
	$("#edit_monthly_target").click(showMonthlyTargetBox);
	$("#edit_daily_target").click(showDailyTargetBox);
	$("#close_btn").click(closeBox);
	$("#sDate, #eDate").change(getReportData);
};

function showDailyTargetBox(){
	$("#machineListForTarget, #close_btn").animate({
		"opacity" :1
	}, function(){
		$("#machineListForTarget").css("z-index",998);
		$("#close_btn").css("z-index",999);
	});
};

function closeBox(){
	$("#machineListForTarget, #close_btn").animate({
		"opacity" :0
	}, function(){
		$("#machineListForTarget").css("z-index",0);
		$("#close_btn").css("z-index",0);
	});
};

function showMonthlyTargetBox(){
	$("#monthlyTargetBox").fadeIn(500);
};

function addMonthlyBlockTarget(){
	var url = ctxPath + "/chart/addMonthlyBlockTarget.do";
	var param = "tgCnt=" + $("#monthlyTargetValue").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success :function(data){
			if(data=="success"){
				$("#monthlyTargetBox").fadeOut(500);
				getMonthlyBlockTatget();
			}
		}
	});
	
};

function selectElements(e) {
    $(document).unbind("mousemove", openSelector);
    $(document).unbind("mouseup", selectElements);
    var maxX = 0;
    var minX = 5000;
    var maxY = 0;
    var minY = 5000;
    var totalElements = 0;
    var elementArr = new Array();
    blocked_icon = new Array();
    $(".init_machine_icon").each(function () {
        var aElem = $(".ghost-select");
        var bElem = $(this);
        var result = doObjectsCollide(aElem, bElem);

        if (result == true) {
        	blocked_icon.push(this.id);
          var aElemPos = bElem.offset();
                var bElemPos = bElem.offset();
                var aW = bElem.width();
                var aH = bElem.height();
                var bW = bElem.width();
                var bH = bElem.height();

                var coords = checkMaxMinPos(aElemPos, bElemPos, aW, aH, bW, bH, maxX, minX, maxY, minY);
                maxX = coords.maxX;
                minX = coords.minX;
                maxY = coords.maxY;
                minY = coords.minY;
                var parent = bElem.parent();

                //console.log(aElem, bElem,maxX, minX, maxY,minY);
                if (bElem.css("left") === "auto" && bElem.css("top") === "auto") {
                    bElem.css({
                        'left': parent.css('left'),
                        'top': parent.css('top')
                    });
                }
          $("body").append("<div id='big-ghost' class='big-ghost' x='" + Number(minX - getElSize(50)) + "' y='" + Number(minY - getElSize(50)) + "'></div>");

            $("#big-ghost").css({
                'width': maxX + getElSize(40) - minX,
                'height': maxY - minY,
                'top': minY - getElSize(10),
                'left': minX - getElSize(20)
            });
          
            init_box_top = minY - getElSize(10);
            init_box_left = minX - getElSize(20);
        }
    });
    
    $(".ghost-select").removeClass("ghost-active");
    $(".ghost-select").width(0).height(0);

    ////////////////////////////////////////////////
    
    $("#big-ghost").draggable({
		start: function(){
			init_icon_offset = new Array();
			
			$(blocked_icon).each(function(idx, data){
				var array = [$("#" + data).offset().left, $("#" + data).offset().top]
				init_icon_offset.push(array);
			});
		},
		drag : function(){
			var offset = $(this).offset();
            var x = offset.left;
            var y = offset.top;
            
            var dx = init_box_left - x;
            var dy = init_box_top - y;
            
            if(dy<0){
            	dy = Math.abs(dy);
            }else{
            	dy = 0 - dy;
            };
            
            if(dx<0){
            	dx = Math.abs(dx);
            }else{
            	dx = 0 - dx;
            };
            
            for(var i = 0; i < blocked_icon.length; i++){
            	$("#" + blocked_icon[i]).css({
            		"left" : init_icon_offset[i][0] + dx,
            		"top" : init_icon_offset[i][1] + dy
            	});
            };
		},
		
		stop : function(){
			$(blocked_icon).each(function(idx, data){
				var offset = $("#" + data).offset();
	            var x = offset.left - marginWidth;
	            var y = offset.top - marginHeight;
	            var id = data.substr(6);
	           setInitCardPos(id, setElSize(x), setElSize(y));
			});
		},
	});

};

var blocked_icon = [];
var init_box_top = 0,
	init_box_left = 0;
var init_icon_offset = [];

function openSelector(e) {
    var w = Math.abs(initialW - e.pageX);
    var h = Math.abs(initialH - e.pageY);

    $(".ghost-select").css({
        'width': w,
        'height': h
    });
    if (e.pageX <= initialW && e.pageY >= initialH) {
        $(".ghost-select").css({
            'left': e.pageX
        });
    } else if (e.pageY <= initialH && e.pageX >= initialW) {
        $(".ghost-select").css({
            'top': e.pageY
        });
    } else if (e.pageY < initialH && e.pageX < initialW) {
        $(".ghost-select").css({
            'left': e.pageX,
            "top": e.pageY
        });
    }
}
  
  
function doObjectsCollide(a, b) { // a and b are your objects
    var aTop = a.offset().top;
    var aLeft = a.offset().left;
    var bTop = b.offset().top;
    var bLeft = b.offset().left;

    return !(
        ((aTop + a.height()) < (bTop)) ||
        (aTop > (bTop + b.height())) ||
        ((aLeft + a.width()) < bLeft) ||
        (aLeft > (bLeft + b.width()))
    );
}  

function checkMaxMinPos(a, b, aW, aH, bW, bH, maxX, minX, maxY, minY) {
    'use strict';

    if (a.left < b.left) {
        if (a.left < minX) {
            minX = a.left;
        }
    } else {
        if (b.left < minX) {
            minX = b.left;
        }
    }

    if (a.left + aW > b.left + bW) {
        if (a.left > maxX) {
            maxX = a.left + aW;
        }
    } else {
        if (b.left + bW > maxX) {
            maxX = b.left + bW;
        }
    }
    ////////////////////////////////
    if (a.top < b.top) {
        if (a.top < minY) {
            minY = a.top;
        }
    } else {
        if (b.top < minY) {
            minY = b.top;
        }
    }

    if (a.top + aH > b.top + bH) {
        if (a.top > maxY) {
            maxY = a.top + aH;
        }
    } else {
        if (b.top + bH > maxY) {
            maxY = b.top + bH;
        }
    }

    return {
        'maxX': maxX,
        'minX': minX,
        'maxY': maxY,
        'minY': minY
    };
}

function enterAdminMode(){
	isAdminMode = true;
	showCorver();
	$("#pwd").val("");
	$("#admin_pwd_box").fadeIn(500);
	$("#pwd").focus();
};

function exitAdminMode(){
	$(".edit").css("display","none");
	disbleDrawIcon();
};

function disbleDrawIcon(){
	$(".init_machine_icon").draggable('disable');
};

function drawStatusPie2(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : getElSize(60),
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
        	text: '장비 상태',
        	style : {
        		color : "white",
        		fontSize : getElSize(50)
        	},
        	y : getElSize(40)
        },
        credits :false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	size:'100%',
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    backgroundColor: 'rgba(252, 255, 197, 1)',
                    borderRadius : 10,
                    borderWidth: 1,
                    borderColor: '#AAA',
                    //format: '{point.y} ',
                    formatter : function(){
                    	var value;
                    	if(this.y!=0){
                    		value = this.y;
                    	}else{
                    		value = null;
                    	}
                    	return value;
                    },
                    style: {
                    	color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
						|| 'black',
                        //textShadow: false ,
                        fontSize : getElSize(60),
                        shadow: {
                            color: 'yellow',
                            width: 10,
                            offsetX: 0,
                            offsetY: 0
                        }

                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'In-Cycle',
                y: 4,
                color : "rgb(104,206,19)"
            }, {
                name: 'Wait',
                y: 1,
                color : "yellow"
            }, {
                name: 'Alarm',
                y: 1,
                color : "red"
            }, {
                name: 'Noconn',
                y: 1,
                color : "gray"
            }], dataLabels: {
                distance: -getElSize(60)
            }
        }]
    });
};


function drawStatusPie(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : getElSize(60),
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
        	text: '가동률',
        	style : {
        		color : "white",
        		fontSize : getElSize(50)
        	},
        	y : getElSize(40)
        },
        credits :false,
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	//borderWidth: 0,
            	size : "100%",
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    //format: '<b>{point.name}</b> : {point.y}%',
                    formatter : function(){
                    	var value;
                    	if(this.key=="가동률"){
                    		value = this.y + "%";
                    	}else{
                    		value = null;
                    	}
                    	return value;
                    },
                    backgroundColor: 'rgba(252, 255, 197, 1)',
                    borderRadius : 10,
                    borderWidth: 1,
                    borderColor: '#AAA',
                    style: {
                    	color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
						|| 'black',
                        //textShadow: false ,
                        fontSize : getElSize(60)
                    }
                }
            }
        },
        series: [{
            colorByPoint: true,
            data: [{
                name: '가동률',
                y: 0,
                color : "rgb(104,206,19)"
            }, {
                name: '비가동',
                y: 0,
                color : "rgba(109,109,109,0)"
            }], dataLabels: {
                distance: -getElSize(100)
            }
        }]
    });
};

var report_opRatio;
function getDvcTemper(dvcId){
	var url = ctxPath + "/chart/getDvcTemper.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			thermometer(data.temperMill, "thermometer");
			thermometer(data.temperLeft, "thermometer2");
			thermometer(data.temperRight, "thermometer3");
		}
	});
};


var opRatio = 0;
var auto_flag = false;
function getReportBarData(){
	 var url = ctxPath + "/chart/getReportBarData.do";
	 var sDate = $("#sDate").val();
	 var eDate = $("#eDate").val();
	 var param = "sDate=" + sDate + 
	 			"&eDate=" + eDate;
	 
	 report_opRatio = 0;
	 $.ajax({
		url : url,
		type : "post",
		data : param,
		dataType : "json",
		success  : function(data){
			var json = data.dataList;
			reportChartName = new Array();
			noconnBar = new Array();
			alarmBar = new Array();
			waitBar = new Array();
			inCycleBar = new Array();
			opTime = 0;
			incycleTime_avg = 0;
			alatmTime_avg = 0;
			opRatio = 0;
			$(json).each(function(idx, data){
				reportChartName.push(data.name);
				//if(data.noconnTime>0){
					noconnBar.push(0);
//				};
				if(data.alarmTime>0){
					alarmBar.push(Number(Number(Number(data.alarmTime)/60/60).toFixed(1)));
				};
				if(data.waitTime>0){
					waitBar.push(Number(Number(Number(data.waitTime)/60/60).toFixed(1)));
				};
				if(data.incycleTime>0){
					inCycleBar.push(Number(Number(Number(data.incycleTime)/60/60).toFixed(1)));
				};
				
				//opTime += Number(data.incycleTime)/60/60/10;
				opRatio += Number(data.opRatio)
				incycleTime_avg += Number(data.incycleTime);
				alatmTime_avg += Number(data.alarmTime);
			});
			
			drawReportColumnChart("columnChart");
			$("#incycleTime_avg").html(Math.floor(incycleTime_avg/60/60/json.length))
			$("#alarmTime_avg").html(Math.floor(alatmTime_avg/60/60/json.length))

			$("#diagram").circleDiagram({
				textSize: getElSize(70), // text color
				percent : Number(opRatio/json.length).toFixed(1) + "%",
				size: getElSize(400), // graph size
				borderWidth: getElSize(30), // border width
				bgFill: "#95a5a6", // background color
				frFill: "#1abc9c", // foreground color
				//font: "serif", // font
				textColor: 'black' // text color
			});
		}
	 });
};

var opTime = 0;
var alatmTime_avg = 0;
var incycleTime_avg = 0;
var noconnBar = [];
var alarmBar = [];
var waitBar = [];
var inCycleBar = [];
function addSeries(){
	reportBar.addSeries({
		color : "gray",
		data : noconnBar
	}, true);
	
	reportBar.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	reportBar.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	reportBar.addSeries({
		color : "green",
		data : inCycleBar
	});
}
var reportChartName = [];
function drawLine(x1,y1, x2,y2, x3,y3, x4,y4){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.lineTo(x4,y4);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	
//	$("#sDate").val(year + "-" + month + "-" + addZero(String(date.getDate()-1)));
	$("#sDate, #eDate").val(year + "-" + month + "-" + day);
};

function getToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	$("#today").html(year + ". " + month + ". " + day + "<br>" + hour + ":" + minute + ":" + second)
	
	setTimeout(getToday,1000);
}
//originally from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
function formatCurrency(n, c, d, t) {
    "use strict";

    var s, i, j;

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    s = n < 0 ? "-" : "";
    i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "";
    j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function thermometer(temper, div) {
	var animate = false;

    var $thermo = $("#" + div),
        $progress = $(".progress", $thermo),
        $goal = $(".goal", $thermo),
        percentageAmount;

    var goalAmount = parseFloat( $goal.text() ),
    progressAmount = Number(temper),
    percentageAmount =  Math.min( Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

    //let's format the numbers and put them back in the DOM
    $goal.find(".amount").text(formatCurrency( goalAmount ) );
    $progress.find(".amount").text(progressAmount + "°C");


    //let's set the progress indicator
    $progress.find(".amount").hide();
    if (animate !== false) {
        $progress.animate({
            "height": percentageAmount + "%"
        }, 1200, function(){
            $(this).find(".amount").fadeIn(500);
        });
    }else{
        $progress.css({
            "height": percentageAmount + "%"
        });
        $progress.find(".amount").fadeIn(500);
    };
};

function drawLineChart(id){
	$('#' + id).highcharts({
		chart : {
			height : getElSize(300),
			backgroundColor : "rgba(0,0,0,0)",
			marginBottom : 0,
			marginLeft :0,
			marginRight:0
		},
		exporting : false,
		credits : false,
        title: {
            text: false,
        },
        subtitle: {
            text: false,
        },
        xAxis: {
        	lineWidth: 0,
        	minorGridLineWidth: 0,
        	minorTickLength: 0,
        	tickLength: 0,
        	lineColor: 'transparent',
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0
        },
        yAxis: {
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0,
            title: {
                text: false
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions : {
        	series : {
        		dataLabels: {
                    enabled: true,
                    style : {
                    	fontSize : getElSize(30),
                    	color : "black",
                    	textShadow: 0
                    },
                    formatter: function () {
                        if (this.point.options.showLabel) {
                            return this.y;
                        }
                        return null;
                    }
                },
            	lineWidth : getElSize(10),
            	marker: {
                    lineWidth: getElSize(10),
                    lineColor: null // inherit from series
                }
            },
        },
        tooltip: {
            //valueSuffix: '°C'
        },
        legend: {
        	enabled : false
        },
        series: []
    });
	
	var chart = $("#lineChart").highcharts()
	var data = Number(Number(Math.random() * 10).toFixed(1));
	chart.addSeries({data:[data], name : "Data"});
	
	for(var i = 0; i < 9; i++){
		data = Number(Number(Math.random() * 10).toFixed(1));
	 	chart.series[0].addPoint(data);
	 	$("#lineChartLabel").html(data);
	};
	
	callback();
};

function callback(){
	var chart = $("#lineChart").highcharts();
	var series = chart.series[0];
    var points = series.points;
    var pLen = points.length;
    var i = 0;
    var lastIndex = pLen - 1;
    var minIndex = series.processedYData.indexOf(series.dataMin);
    var maxIndex = series.processedYData.indexOf(series.dataMax);

    points[minIndex].options.showLabel = true;
	points[maxIndex].options.showLabel = true;
	//  points[lastIndex].options.showLabel = true;
	series.isDirty = true;
	chart.redraw();
};

function drawReportColumnChart(id){
	Highcharts.createElement('link', {
		   href: '//fonts.googleapis.com/css?family=Unica+One',
		   rel: 'stylesheet',
		   type: 'text/css'
		}, null, document.getElementsByTagName('head')[0]);
	
	 $('#' + id).highcharts({
		 	chart : {
		 		height : getElSize(1100),
		 		type: 'column',
		 		backgroundColor : "white",
		 		 style: {
		 	         fontFamily: "'Unica One', sans-serif"
		 	      },
		 	},
	        title: {
	            text: false,
	        },
	        exporting : false,
	        credits : false,
	        subtitle: {
	            text: false,
	        },
	        xAxis: {
	            categories: reportChartName,
                labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(50)
	   	           },
	            },
	        },
	        yAxis: {
	        	max : 24,
	        	step : 2,
	            title: {
	                text: false
	            },
	            labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(30)
	   	           },
//	   	           formatter: function () {
//	   	        	   if(this.value%2==0) return this.value;
//	   	           }
	            },
	        },
	        tooltip: {
	            enabled :false
	        },
	        plotOptions: {
	            series: {
	                lineWidth: getElSize(15),
	                borderWidth: 0,
	            },
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                	formatter : function(){
	                		if(this.y!=0){
	                			return this.y;
	                		}else{
	                			return null;
	                		};
	                	},
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                        fontSize : getElSize(30),
	                        textShadow: '0 0 3px white'
	                    }
	                }
	            },
	        },
	        legend: {
	        	enabled : false
	        },
	        series: []
	         
	    });
	 
	 Highcharts.setOptions(Highcharts.theme);
	 
	 reportBar = $("#" + id).highcharts();
	 
	 addSeries();
};
var reportBar;
var block = 1/6;

function drawRect(x,y,w,h){
//	var w = getElSize(400);
//	var h = getElSize(80);
//	var x = (contentWidth/2) - (w/2);
//	var y = $(".mainTable").height() - marginHeight;
	
	ctx.strokeStyle = "gray";
	ctx.lineWidth = 0.3;
	ctx.rect(x,y+$(".mainTable").height() - marginHeight,w,h);
	ctx.lineJoin = 'round';
	ctx.stroke();
	
	ctx.fillStyle = "black";
	ctx.font = getElSize(40) + "px Calibri";
	//ctx.fillText("Reception Desk",x+getElSize(70),y + getElSize(40));
	
	ctx.fillStyle = "white";
	ctx.font = getElSize(33) + "px Calibri";
	//ctx.fillText("Staff Lounge Storage",getElSize(3520), getElSize(1050) - marginHeight);
	
};

function getAllMachine(){
	var url = ctxPath + "/chart/getAllBlockMachine.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.machineList;

			var l_x1,l_y1,l_y2,l_y2;
			var r_x1,r_y1,r_y2,r_y2;
			var t_x1,t_y1,t_x2,t_y2,t_x3,t_y3;

			var icon = "";
			var class_name = "";
			var status = "";
			var machineName = "";
			var border_color = "";
			var fontColor = "white";
			
			var specialName = "";
			$(json).each(function(idx, data){
				if((data.status==null || data.status.toLowerCase()=="no-connection") && data.isEmpty==0){
					status = "NOCONN.svg";
					noConnMachine++;
					border_color = "gray";
					fontColor = "white";
				}else if(data.status.toLowerCase()=="wait"){ 
					status = "WAIT.svg";
					waitMachine ++;
					border_color = "yellow";
					fontColor = "black";
				}else if(data.status.toLowerCase()=="alarm"){
					status = "ALARM.svg";
					alarmMachine ++;
					border_color = "red";
					fontColor = "white";
				}else if(data.status.toLowerCase()=="in-cycle"){
					status = "IN-CYCLE.svg";
					incycleMachine++;
					border_color = "green";
					fontColor = "white";
				}else{
					border_color = "blank";
					fontColor = "white";
				}
				
				var isEmpty = data.isEmpty;
				if(isEmpty==1){
					status = ".svg";
				}
				
				if(data.name.indexOf("AL")==-1 && data.name.indexOf("MCT")!=-1){
					machineName = decodeURIComponent(data.name).replace(/\+/gi," ").substr(3);
				}else{
					machineName = "";
				}
				
				if(machineName=="21"){
					specialName = "<span>특주</span>"; 
				}else{
					specialName = "";
				}
				
				if(data.display==1){
					class_name = "display";
				}else{
					class_name = "hide";
				};
				
				var map = new JqMap();
				map.put("id", data.id);
				map.put("name", data.name);
				map.put("status", border_color);
				map.put("pic", data.pic);
				map.put("endThree", data.endThree);
				map.put("endFive", data.endFive);
				
//				map.put("opRatio", data.opRatio);
//				map.put("incycleTime", data.incycleTime);
//				map.put("waitTime", data.waitTime);
//				map.put("alarmTime", data.alarmTime);
//				map.put("noconnTime", data.noconnTime);
				
				card_bar_data_array.push(map);
				
				icon += "<div class='init_machine_icon " + class_name + "' id='circle" + data.id + "' style='left : " + (getElSize(data.x) + marginWidth) + "; top:" + (getElSize(data.y) + marginHeight) +"; width :" + (getElSize(data.w)*1.4) + "';>" +
							"<span class='machine_name' id='machine_name_" + data.id + "' style='color:" + fontColor + "; font-size:" + getElSize(60) + "'>" + machineName + "</span>" +
							"<span class='machine_name'  style='color:" + fontColor + "; font-size:" + getElSize(40) + "; margin-top : " + getElSize(70) +"'>" + specialName + "</span>" +
							"<img src=" +  ctxPath + "/images/THK/MCT/"+ data.pic + "_" + status + "?dummy=" + new Date().getMilliseconds() + " class='init_icon' id=img" + data.id + " style='    transform: rotate(" + data.rotate + "deg);'>" +
						"</div>";
			});
			
			drawRect(getElSize(1000), getElSize(50), getElSize(2200),getElSize(600)); drawRect(getElSize(3350), getElSize(50), getElSize(450),getElSize(600));
			drawRect(getElSize(1000), getElSize(800), getElSize(2200),getElSize(500)); drawRect(getElSize(3350), getElSize(800), getElSize(450),getElSize(500));

			$("#main_table").css({
				"display" : "inline"
			});
			
			$("#svg").html(icon);
			setEl();
			
			stateBorder();
			
			$(".init_icon").css({
				"width": "100%",
				//"margin-top" : getElSize(50),
			});
			
			$(".machine_name").each(function(idx, data){
				var svg = $(data).parent().children("img");
				$(data).css({
					"position" : "absolute",
					"left" : (svg.width()/2) - ($(data).width()/2),
					"top" : (svg.width()/3.4/2)/2,
					"z-index" : 2,
					"font-weight" : "bolder"
				});
			});
			
			$(".init_icon").css({
				"width": "100%",
				//"margin-top" : getElSize(50),
			});
			
			$(".init_icon").each(function(idx, data){
				$(data).css({
					"left" : (circleWidth/2) - $(data).width()/2,
					"top" : (circleWidth/2) - $(data).height()/2,
					"user-drag": "none" 
				});
			});
			
			
			$(".init_machine_icon").dblclick(function(){
				var id = this.id.substr(6);
				window.localStorage.setItem("dvcId", id);
				var url = ctxPath + "/chart/detailChart_block.do";
				location.href = url;
			});
			//setTimeout(resetIcon, 3000);
			
			getMachineStatus();
		}
	});
	
};

function enableDrawIcon(){
	$(".init_machine_icon").draggable({
		start: function(){
		},
		stop : function(){
			var offset = $(this).offset();
            var x = offset.left - marginWidth;
            var y = offset.top - marginHeight;
            var id = this.id;
            id = id.substr(6);
            
           setInitCardPos(id, setElSize(x), setElSize(y));
		},
	});
	
	$("#grid").mousedown(function (e) {
        $("#big-ghost").remove();
        $(".ghost-select").addClass("ghost-active");
        $(".ghost-select").css({
            'left': e.pageX,
            'top': e.pageY
        });

        initialW = e.pageX;
        initialH = e.pageY;

        $(document).bind("mouseup", selectElements);
        $(document).bind("mousemove", openSelector);

    });
};

function disbleDrawIcon(){
	$(".init_machine_icon").draggable('disable');
};

function drawTriangle(x1, y1, x2, y2, x3, y3){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.lineTo(x1,y1);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

var displayMachine = new Array();
function resetIcon(){
	$(".hide").animate({
		"opacity" : 0
	},1000);
	
	reArrangeIcon();
};

function setInitCardPos(id, x, y){
	var url = ctxPath + "/chart/setInitCardPos.do";
	var param = "id=" + id + 
				"&x=" + x + 
				"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){}
	});
};

function csvSend(){
	
	var sDate, eDate;
	var csvOutput;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	csvOutput = csvData;
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};


function statusBarAnim(chart, idx){
	var startPoint = $($(".highcharts-grid")[idx]).offset().left;
	var endPoint = $("#" + chart).offset().left + $("#" + chart).width()-10;
	var width = endPoint - startPoint;
	var linePos = width/24;
	
	var date = new Date();
	var hour = date.getHours();
	var minute = String(date.getMinutes());
	
	if(minute.length==1){
		minute = 0;
	}else{
		minute = minute.substr(0,1);
		minute = (linePos/6) * minute;
	};

	if(hour<20){
		linePos = (hour+4) * linePos + startPoint + minute;
	}else{
		linePos = (hour-20) * linePos + startPoint + minute;
	};
	
	$("#verticalLine" + idx).css({
		"left" : linePos,
		"height" : $("#status3_1").height()-45,
		"z-index" : 999999999,
		"display" : "inline"
	});
	
	$("#verticalLine" + idx).css("box-shadow", "0px 0px " + lineWidth + "px " + lineWidth + "px rgb(86,215,250)");

	if (toggle) {
		lineWidth -= getElSize(5)/5;
		if (lineWidth <= 0) {
			toggle = false;
		};
	} else if (!toggle) {
		lineWidth += getElSize(5)/5;
		if (lineWidth >= getElSize(5)) {
			toggle = true;
		};
	};
		
	setTimeout(function(){
		statusBarAnim(chart, idx);
	}, 80)
};

var lineWidth = getElSize(5);
var toggle = true;

var alarmList = new Array();
var machineList = new Array();
var cardMachine = new Array();

function reArrangeIcon(){
	$(cardMachine).each(function(idx, data){
		$("#circle" + data[0]).animate({
			"left" : getElSize(data[1]) + marginWidth,
			"top" : getElSize(data[2]) + marginHeight + getElSize(100)
		}, 1000, function(){
			$("#circle" + data[0] + ", #canvas").animate({
				"opacity" : 0
			});
		});
	});
	
	setTimeout(function(){
		$("#svg").hide();
		$("#cards").animate({
			"opacity" : 1
		});
		autoSlide();
		clearInterval(border_interval);
		stateBorder();
	}, 1500);
	
	getMachineStatus();
	
	//setInterval(getMachineStatus, 1000*5);
};

function setCard(){
	setEl();
	
	$(".removeIcon").css({
		"width" : getElSize(100),
		"display" : "none"
	});
	
	$(".removeIcon").click(removeCard);
	
	$(".icon").css("width" , contentWidth * 0.07 * 0.8);
	
	$(card_bar_data_array).each(function(idx, data){
		statusBar("card_status" + data.get("id"), data.get("id"));
		statusBarTotal("card_status" + data.get("id") + "_total", data.get("id"));
	});
	
	//$(".machine_cam").not("img").click(viewCam);
	
	//$(".icon").click(flipCard);
	
	$(".wrap").css({
		"transition" : "0.5s",
	});
	
	$(card_bar_data_array).each(function(idx, data){
		setDiagram(data.get("id"), data.get("opRatio"));
	});
};

function setDiagram(id, ratio){
	$("#diagram" + id).circleDiagram({
		textSize: getElSize(50), // text color
		percent : Number(ratio) + "%",
		size: getElSize(170), // graph size
		borderWidth: getElSize(20), // border width
		bgFill: "#95a5a6", // background color
		frFill: "#1abc9c", // foreground color
		//font: "serif", // font
		textColor: 'black' // text color
	});
};

var machine_card = "";
var card_bar_data_array = [];
var new_card_bar_data_array = [];
var status_interval = null;

var incycleMachine = 0,
	waitMachine = 0,
	alarmMachine = 0,
	noConnMachine = 0;

var intervalArray = new Array();

function animIcon(id, dur){
	var array = [id, "interval" + id, false];
	var dupl = false;
	for(i = 0; i < intervalArray.length; i++){
		if(intervalArray[i][0] == id){
			intervalArray[i] = array;
			dupl = true;
		}
	};
	
	if(!dupl) intervalArray.push(array);
	
	var opacity = 0;
	
	var idx;
	for(i = 0; i < intervalArray.length; i++){
		if(intervalArray[i][0] == id){
			idx = i;
		};
	};
	
	intervalArray[idx][1] = setInterval(function(){
		if(intervalArray[idx][2]){
			opacity = 1;
		}else{
			opacity = 0;
		};
		$("#" + id).animate({
			"opacity" : opacity
		}, dur);
		intervalArray[idx][2] = !intervalArray[idx][2];
	},dur);
};

function clearAnim(id){
	for(var i = 0; i < intervalArray.length; i++){
		if(intervalArray[i][0]==id){
			clearInterval(intervalArray[i][1]);
			$("#" + id).animate({
				"opacity" : 1
			}, 300);
			return;
		};
	};
};

function getMachineStatus(){
	incycleMachine = 0,
	waitMachine = 0,
	alarmMachine = 0,
	noConnMachine = 0;
	
	var url = ctxPath + "/chart/getMachineStatus.do";
	
	$.ajax({
		url : url,
		dataType: "json",
		type : "post",
		success :function(data){
			var json = data.machineList;
			new_card_bar_data_array = [];
			var status = "";
			$(json).each(function(idx, data){
				if((data.status==null || data.status.toLowerCase()=="no-connection") && data.isEmpty==0){
					status = "NOCONN.svg";
					noConnMachine++;
					border_color = "gray";
				}else if(data.status.toLowerCase()=="wait"){ 
					status = "WAIT.svg";
					waitMachine ++;
					border_color = "yellow";
				}else if(data.status.toLowerCase()=="alarm"){
					status = "ALARM.svg";
					alarmMachine ++;
					border_color = "red";
				}else if(data.status.toLowerCase()=="in-cycle"){
					status = "IN-CYCLE.svg";
					incycleMachine++;
					border_color = "green";
				}else{
					border_color = "blank"
				}
				
				var map = new JqMap();
				map.put("id", data.id);
				map.put("status", border_color);
				map.put("pic", data.pic);
				map.put("icon", status);
				map.put("endThree", data.endThree);
				map.put("endFive", data.endFive);
				
				if(data.endFive=="ON"){
					clearAnim("circle" + data.id);
					animIcon("circle" + data.id,1000);
				};
				
				if(data.endThree=="ON"){
					clearAnim("circle" + data.id);
					animIcon("circle" + data.id,300);
				};
				
					
				new_card_bar_data_array.push(map);
			});
			
			//status 변화 비교
			var statusChagne = false;
			for(var i = 0; i < new_card_bar_data_array.length; i++){
				if(card_bar_data_array[i].get("status") != new_card_bar_data_array[i].get("status")){
					statusChagne = true;
					$("#circle" + new_card_bar_data_array[i].get("id") + " img").attr("src", ctxPath + "/images/THK/MCT/"+ new_card_bar_data_array[i].get("pic") + "_" + new_card_bar_data_array[i].get("icon") + "?dummy=" + new Date().getMilliseconds());
					
					$("#machine_name_" + card_bar_data_array[i].get("id")).css("color", "white");
					if(new_card_bar_data_array[i].get("status").toUpperCase()=="YELLOW"){
						$("#machine_name_" + card_bar_data_array[i].get("id")).css("color", "black");
					}
				};
			};
			
			card_bar_data_array = new_card_bar_data_array;
			
			if(statusChagne){
				console.log("reset")
				stateBorder();
			};
			
//			clearInterval(border_interval);
//			stateBorder();
			
			clearInterval(status_interval);
			status_interval = setInterval(getMachineStatus,5000);
			reDrawPieChart();
		}
	});
};

function reDrawPieChart(){
	getTotalOpRatio();	

	var pie2 = $("#status_pie2").highcharts();
	pie2.series[0].data[0].update(incycleMachine);
	pie2.series[0].data[1].update(waitMachine);
	pie2.series[0].data[2].update(alarmMachine);
	pie2.series[0].data[3].update(noConnMachine);
	
};

function getTotalOpRatio(){
	var url = ctxPath + "/chart/getTotalOpRatio.do";
	var param = "type=block";
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var sum = 0;
			$(json).each(function(idx, data){
				sum += Number(data.opRatio);
			});
			
			sum = Number(Number(sum/json.length).toFixed(1));
			
			var pie1 = $("#status_pie").highcharts();
	
			pie1.series[0].data[0].update(sum);
			pie1.series[0].data[1].update(100-sum);
		}
	});
};

var today;

var demo_status = [];
function select_remove_card(){
	$(".removeIcon").toggle(500);
};

function removeCard(){
	var del_confirm = confirm("Are you sure you want to delete?");
	if(!del_confirm) return;
	
	var url  = ctxPath + "/chart/removeCard.do";
	var id = this.id.substr(1);
	var param = "id=" + id;
	$("#wrap" + id).toggle(500);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){}
	});
};

var hold_time = 1000;
var timeout_id = 0;
var border_flag = true;

function borderAnim(){
	var border;
	if(border_flag){
		border = getElSize(20);
	}else{
		border = 0
	};
	$(alarmList).each(function(idx, data){
		$("#" + data).css({
			"box-shadow" : "0px 0px " + border + "px " + border + "px red"
		});
	});
	
	border_flag = !border_flag;
	setTimeout(borderAnim,300);
};

function stateBorder(){
	clearInterval(border_interval);
	var color = "green";
	
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("status")=="yellow"){
			color = "yellow";
		}
	};
	
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("status")=="red"){
			color = "red";
		}
	};
	
	border_interval = setInterval(function(){
		var border;
		if(border_flag){
			border = getElSize(20);
		}else{
			border = 0
		};
		
		border_flag = !border_flag;
		
		$("#stateBorder").css({
			"transition" : "0.5s",
			"box-shadow" : "inset 0px 0px " + border + "px " + border + "px " + color
		});
	}, 300);
};

var border_interval = null;
var cam_width, cam_height, cam_top, cam_left;

var cam_flag = false;


function viewCam(){
	var $cam = $("#" + this.id);
	if(!cam_flag){
		//var src = $cam.attr("src");
		cam_width = $cam.width();
		cam_height = $cam.height();
		cam_top = $cam.offset().top;
		cam_left = $cam.offset().left;
		
		$cam.appendTo("body");
		
		$cam.css({
			"position": "absolute",
			"top" : cam_top,
			"left" : cam_left,
			"width" : cam_width,
			"height" : cam_height,
			"z-index" : 99999
		});
			
		$cam.animate({
			"top" : 0,
			"left" : 0,
			"height" : originHeight,
			"width" : originWidth
		}, function(){
			if($cam.hasClass("video")) $cam.get(0).play();
		});
		
		//회사 로고 & 모델명
		var idx = this.id.substr(3);
		var company;
		var model_name;
		for(var i = 0; i < machineList.length; i++){
			if(machineList[i][0]=="wrap" + idx){
				company = machineList[i][1];
				model_name = machineList[i][2];
			}
		};
		
		var logo = "<img src=" + ctxPath + "/images/company/" + company.toLowerCase() + ".png class='logo_zoom' style='height:5%' >";
		var model = "<div  style='color:white;' class='model_name'>" + model_name + "</div>"; 
		$("body").prepend(logo);
		$("body").prepend(model);
		
		$(".logo_zoom").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left,
			"top" : cam_top
		});
		
		$(".model_name").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left + cam_width - $(".model_name").width(),
			"top" : cam_top,
			"font-size" : getElSize(70)
		});
		
		$(".logo_zoom").animate({
			"top" : 0,
			"left" : 0
		});
		
		$(".model_name").animate({
			"top" : 0,
			"left" : originWidth - ($(".model_name").width() + getElSize(50))
		});
		
	}else{
		closeCam($cam);
	};
	cam_flag = !cam_flag;
};

function closeCam(obj){
	$(".logo_zoom").remove();
	$(".model_name").remove();

	$(obj).animate({
		"width" : cam_width,
		"height" : cam_height,
		"top" : cam_top,
		"left" : cam_left
	}, function(){
		var id = obj.selector.substr(4);
		$("#cam" + id).appendTo("#wrap" + id + " .front .card_table tr:nth(1) td:nth(1)");
		$(obj).css("position","static");
		
		//if($(obj).hasClass("video")) obj.get(0).play();
	});
};

function setCardPos(id, x, y){
	console.log(x, y)
	var url = ctxPath + "/chart/setEmoCardPos.do";
	var param = "id=" + id +  
				"&x=" + x +
				"&y=" + y;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success :function(data){
			
		}
	});
};

function statusBarTotal(id, idx){
	
	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	var noconn = 0;
	
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("id")==idx){
			incycle = card_bar_data_array[i].get("incycleTime");
			wait = card_bar_data_array[i].get("waitTime");
			alarm = card_bar_data_array[i].get("alarmTime");
			//noconn = card_bar_data_array[i].get("noconnTime");
		}
	};
		
		
	
	incycle = Number(Number(incycle/60/60).toFixed(1));
	wait = Number(Number(wait/60/60).toFixed(1));
	alarm = Number(Number(alarm/60/60).toFixed(1));
	noconn = Number(Number(noconn/60/60).toFixed(1));
	
	var sum = incycle + wait + alarm;
	
	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'green' ], [ 1, 'green' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'gray' ], [ 1, 'gray' ] ]
		}, ]
		
	
	$('#' + id).highcharts({
        chart: {
            type: 'bar',
            marginLeft:0,
            marginRight:0,
            marginTop:0,
            marginBottom:0,
          //  width : getElSize(785),
            backgroundColor : "rgba(0,0,0,0)"            	
        },
        credits : false,
        exporting : false,
        title: {
            text: ''
        },
        xAxis: {
        	labels:{
            	enabled : false
            },
            tickLength: 0
        },
        yAxis: {
            min: 0,
            //max:Math.floor(sum),
           max:24,
            gridLineWidth: 0,
            reversedStacks: false,
            title: {
                text: ''
            },
            labels:{
            	enabled : true
            }
        },
        legend: {
        	enabled:false
        },
        plotOptions : {
        	bar: {
                stacking: 'normal',
                dataLabels: {
                	style : {
                		fontSize : getElSize(20),
                		textShadow: 0
                	},
                	enabled: true,	
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                    	var val = this.y
//                    	if(val<=1){
//                    		val = "";
//                    	};
                    	return val;   
                    }
                }
        	},	
			series : {
				stacking : 'normal',
				pointWidth : getElSize(50),
				borderWidth : 0,
				animation : false,
				cursor : 'pointer',
			}
		},
		tooltip : {
			enabled : false
		},
        series: [{
        	color : "green",
        	data : [0]
        },
        {
        	color : colors[1],
        	data : [0]
        },
        {
        	color : colors[2],
        	data : [0]
        },
        {
        	color : "gray",
        	data : [0]
        }]
    });
	
		
	var chart = $("#" + id).highcharts();
	
	chart.series[0].data[0].update(incycle);
	chart.series[1].data[0].update(wait);
	chart.series[2].data[0].update(alarm);
//	chart.series[3].data[0].update(noconn);
	
	//Number(Number(alarm/sum*10).toFixed(1))
};

var barChart
var test_options;
function statusBar(id, idx){
	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'green' ], [ 1, 'green' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'gray' ], [ 1, 'gray' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(0,0,0,0)',
				height :getElSize(50),
				margin : 0,
				marginTop : -getElSize(500)
			},
			credits : false,
			title : false,
			xAxis : {
				categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
						0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
						0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
						0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
						0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
						0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
						0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
						18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
				labels : {

					formatter : function() {
						var val = this.value
						if (val == 0) {
							val = "";
						}
						;
						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(15),
						fontWeight : "bold"
					},
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				}
			},
			legend : {
				enabled : false
			},
			series : []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		var options = status.options;
		option = options;
		
//		var date = new Date();
//		var hour = date.getHours();
//		var minute = addZero(String(date.getMinutes()));
//		minute = minute.substr(0,1);
//		
//		if(hour>=20){
//			hour -= 20;
//		
//		}else{
//			hour += 4;
//		};
//		
//		hour *= 6;
//		
//		hour+=Number(minute);
//		
//		var color = "";
//		
//		var array = [];
//		array.push(idx);
//		
//		for(var i = 0; i < hour; i++){
//			var rnd = Math.floor(Math.random()*10);
//			
//			if(rnd<=5){
//				color = "green";
//			}else if(rnd<=8){
//				color = "yellow";
//			}else{
//				color = "red";
//			};
//			array.push(color);
//		};
//		
//		statusColor.push(array);
		
		options.series = [];
		options.title = null;
		options.exporting = false;

//		var dvcStatus;
//		
//		for(var i = 0; i < statusColor.length; i++){
//			if(statusColor[i][0]==idx){
//				dvcStatus = statusColor[i]; 
//			}
//		}
//		options.series.push({
//			data : [ {
//				y : Number(20),
//				segmentColor : dvcStatus[1]
//			} ],
//		});
//			
//		
//		
//		for(var i = 2; i < dvcStatus.length; i++){
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : dvcStatus[i]
//			});
//		};
//		
//		for(var i = 1; i < 145-dvcStatus.length; i++){
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : "white"
//			});
//		};
		
		
		getTimeData(idx, options);
		//status = new Highcharts.Chart(options);
};

function getTimeData(id, options){
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	
	if(hour>=20){
		day = addZero(String(new Date().getDate()+1));
	};
	
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" + today + 
				"&dvcId=" + id;
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			
//			options.series.push({
//				data : [ {
//					y : Number(20),
//					segmentColor : "gray"
//				} ],
//			});
//			
//			for(var i = 0; i < 23; i++){
//				options.series[0].data.push({
//					y : Number(20),
//					segmentColor : "gray"
//				});
//			};
			
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = "green"
			}else if(status=="WAIT"){
				color = "yellow";
			}else if(status=="ALARM"){
				color = "red";
			}else if(status=="NO-CONNECTION"){
				color = "gray";
			};
			
			options.series.push({
				data : [ {
					y : Number(20),
					segmentColor : color
				} ],
			});
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = "green"
				}else if(data.status=="WAIT"){
					color = "yellow";
				}else if(data.status=="ALARM"){
					color = "red";
				}else if(data.status=="NO-CONNECTION"){
					color = "gray";
				};
				options.series[0].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 144-json.length; i++){
				options.series[0].data.push({
					y : Number(20),
					segmentColor : "rgba(0,0,0,0)"
				});
			};
			
			status = new Highcharts.Chart(options);
		}
	});
};

var statusColor = [];
var labelsArray = [ 20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20 ];

function parsingAlarm(str){
	var alarm = JSON.parse(str);
	
	var alarmMsg = " ";
	$(alarm).each(function(idx, data){
		if(data.ALARMMSG!="NULL" && typeof(data.ALARMMSG)!="undefined") alarmMsg += data.ALARMCODE + " - " + data.ALARMMSG + "<br>";
	});
	
	$("#alarm").css({
		"height" : getElSize(200)
	})
	
	$("#alarm").html(alarmMsg);
};

var card_width;
var card_height;
var card_top;
var card_left;
var spd_feed_interval = null;
function flipCard() {
	var $wrap = $(this).parent().parent().parent().parent().parent().parent(); 
	var idx = this.id.substr(3);
	var img = "<img src=" + ctxPath + "/images/company/doosan.png style='height:20%'>";
	

	
	//spd_feed_interval = setInterval(function(){
		for(var i = 0; i < card_bar_data_array.length; i++){
			if(card_bar_data_array[i].get("id")==idx){
				$("#machine_name").html(card_bar_data_array[i].get("name"));
				$(".neon1").html(Number(card_bar_data_array[i].get("opRatio")));
				$(".neon2").html(Number(card_bar_data_array[i].get("motorEndUpMovement")));
				$(".neon3").html(Number(card_bar_data_array[i].get("motorUpDownMovement")));
			}
		};
	//},3000);
	
		
	pieChart(idx);
	drawBarChart("container", idx);
		
	$wrap.css({
		"-webkit-transform" : "rotateY(180deg)",
	});

	var div = document.createElement("div");
	div.setAttribute("id", "back");
	var top = $wrap.offset().top;
	var left = $wrap.offset().left;
	var width = $wrap.width();
	var height = $wrap.height();

	var width_padding = Number($wrap.css("padding-left").substr(0,
			$wrap.css("padding-left").lastIndexOf("px"))) * 2;
	var height_padding = Number($wrap.css("padding-top").substr(0,
			$wrap.css("padding-top").lastIndexOf("px")))
			+ Number($wrap.css("padding-bottom").substr(0,
					$wrap.css("padding-bottom").lastIndexOf("px")));

	card_width = width + width_padding;
	card_height = height + height_padding;
	card_top = $wrap.offset().top;
	card_left = $wrap.offset().left;

	div.style.cssText = "background-color:white; " + "z-index:9999999; "
			+ "position:absolute; " + "top : " + top + "; " + "left:" + left
			+ "; " + "width:" + (width + width_padding) + "; " + "height:"
			+ (height + height_padding) + ";" + "border-radius : "
			+ getElSize(30) + "px " + 
			"background-images:url(../images/shopLayOut/card_back.png";

	$("#mainTable2").css("opacity", 0);

	setTimeout(function() {
		$("body").prepend(div);
		$(div).click(flipCard_r);
		$(div).animate({
			"border-radius" : 0,
			"width" : window.innerWidth,
			"height" : window.innerHeight,
			"top" : 0,
			"left" : 0,
			"background-color" : "rgb(234,234,234)",
		}, 500, function() {
			$(div).append($("#mainTable2"));
			
			for(var i = 0; i < machineList.length; i++){
				if(machineList[i][0]=="wrap" + idx)idx = i;
			};
			var machine_name = machineList[idx][2];
			var img = "<img src=" + ctxPath + "/images/company/" + machineList[idx][1].toLowerCase() + ".png style='height:20%'>";

			$("#mainTable2").animate({
				"opacity" : 1
			}, 500);
			
			$("#machine_name").css({
				"margin-top" : $("#machine_name_td").height() / 2
								- $("#machine_name").height() / 2
								- $(".subTitle").height()
			});
		});
	}, 500)
};

function hideRemoveBtn(){
	if(!rm_btn) return;	
	//$(".removeIcon").hide(500);
};

var rm_btn = false;
function evtBind() {
	cPage = $("#panel_table td img:nth(0)").attr("id");
	//$("div").not($(".wrap")).click(hideRemoveBtn);
	
	$("#menu_btn").click(togglePanel);
	$("#corver").click(function() {
		//if (panel)togglePanel();
	});
	
	$("#panel_table td img,#panel_table td div ").click(slidePage);
};

var upPage = new Array();
var downPage = new Array();
var cPage;

function slidePage(){
	$("#panel_table img, #panel_table div").css("border",0)
	$(this).css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	var id = this.id;
	
	if(cPage==id){
		$("#menu_btn").click();
		return;
	};
	
	upPage.push(cPage);
	var page = cPage.substr(cPage.lastIndexOf("_")+1);
	
	$("#part" + page).animate({
		"top" : - originHeight
	});
	
	cPage = id;
	
	page = id.substr(cPage.lastIndexOf("_")+1);

	$("#part" + page).animate({
		"top" : 0
	});
	
	if(page==3){
		$(".thead").animate({
			"top" : $(".thead").offset().top - originHeight  
		})
	}else{
		$(".thead").animate({
			"top" : $(".thead").offset().top + originHeight  
		})
	};
	
	togglePanel();
};

function showCorver(){
	$("#corver").css({
		"z-index":4,
		"opacity":0.7
	});
};

function hideCorver(){
	$("#corver").css({
		"z-index":-1,
		"opacity":0
	});
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};


function drawBarChart(id, idx) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]

	var height = window.innerHeight;

	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : '#ffffff',
			height : height * 0.38,
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
					0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
					0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
					0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
					0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
					0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
					0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
					18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
			labels : {

				formatter : function() {
					var val = this.value
					if (val == 0) {
						val = "";
					};
					return val;
				},
				style : {
					color : "black",
					fontSize : getElSize(15),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#container").highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;

//	var mahcine_status;
//	
//	for(var i = 0; i < statusColor.length; i++){
//		if(statusColor[i][0]==idx){
//			mahcine_status = statusColor[i] 
//		}
//	};
//	
//	options.series.push({
//		data : [ {
//			y : Number(20),
//			segmentColor : mahcine_status[1]
//		} ],
//	});
//		
//	
//	
//	
//	for(var i = 2; i < mahcine_status.length; i++){
//		options.series[0].data.push({
//			y : Number(20),
//			segmentColor : mahcine_status[i]
//		});
//	};
//	
//	for(var i = 1; i < 145-mahcine_status.length; i++){
//		options.series[0].data.push({
//			y : Number(20),
//			segmentColor : "rgba(0,0,0,0)"
//		});
//	};
//	

	getTimeData(idx, options)
};

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':'
			+ sec;
	return time;
}

function pieChart(idx) {
	Highcharts.setOptions({
		// green yellow red gray
		colors : [ 'rgb(100,238,92 )', 'rgb(250,210,80 )', 'rgb(231,71,79 )',
				'#8C9089' ]
	});

	$('#pie')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',
							backgroundColor : "rgba(0,0,0,0)"
						},
						credits : false,
						exporting : false,
						title : {
							text : false
						},
						legend : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'black',
										fontSize : getElSize(40),
										textShadow: 0
									}
								},
								showInLegend : true
							}
						},
						series : [ {
							name : "Brands",
							colorByPoint : true,
							data : [ {
								name : "In-Cycle",
								y : 0
							}, {
								name : "Wait",
								y : 0
							}, {
								name : "Alarm",
								y : 0
							}, {
								name : "No-Connection",
								y : 0
							}]
						} ]
					});
	
	var chart = $("#pie").highcharts();
	
	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	var noconn = 0;

	console.log(card_bar_data_array)
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("id")==idx){
			incycle = Number(card_bar_data_array[i].get("incycleTime"));
			wait = Number(card_bar_data_array[i].get("waitTime"));
			alarm = Number(card_bar_data_array[i].get("alarmTime"));
			noconn = Number(card_bar_data_array[i].get("noconnTime"));
		}
	};
		
	var sum = incycle + wait + alarm + noconn;	
	
	chart.series[0].data[0].update(Number(Number(incycle/sum*100).toFixed(1)));
	chart.series[0].data[1].update(Number(Number(wait/sum*100).toFixed(1)));
	chart.series[0].data[2].update(Number(Number(alarm/sum*100).toFixed(1)));
	//chart.series[0].data[3].update(Number(Number(noconn/sum*100).toFixed(1)));
};

function setEl() {
	$("body").css({
		"overflow" : "hidden"
	});
	
	$("#canvas, #canvas_block").css({
		"position" : "absolute",
		"background" : "whtie",
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$("#stopListTable").css({
		"position" : "absolute",
		"width" : getElSize(950),
		"display" : "none"
	});
	
	$("#stopListTable td").css({
		"color" : "white",
		"border" : "solid 1px gray",
		"padding" : getElSize(5),
		"font-size" : getElSize(40)
	});
	
	$(".disable_dvc_name").css({
		"color" : "white"
	});
	
	
	$("#status_pie, #status_pie2").css({
		"width" : getElSize(500),
		"height" : getElSize(480),
	});
	
	$("#time_table").css({
		"color" : "white",
		"position" : "absolute",
		"right" : marginWidth + getElSize(20),
		"top" : marginHeight + getElSize(20),
		"text-align" : "right",
		"font-size" : getElSize(40),
		//"z-index" : 1
	});
	
	$("#admin_pwd_box").css({
		"position" : "absolute",
		"display" : "none",
		"z-index" : 9,
		"background-color" : "rgb(34,34,34)",
		"color" : "white",
		"padding" : getElSize(20),
		"font-size" : getElSize(50)
	});
	
	$("#admin_pwd_box").css({
		"left" : (originWidth/2) - ($("#admin_pwd_box").width()/2),
		"top" : (originHeight/2) - ($("#admin_pwd_box").height()/2)
	});
	
	$("#pwd").css({
		"width" : getElSize(500),
		"height" : getElSize(100),
		"font-size" : getElSize(50),
	});
	
	$("#main_table").css({
		"position" : "absolute",
		"top" : marginHeight + $("#mainTable").height() + getElSize(50),
		"left" : marginWidth + getElSize(30),
		"width" : getElSize(950),
	});
	
	$("#main_table td").css({
//		"color" : "white",
//		"border" : "solid 1px gray",
//		"padding" : getElSize(5),
//		"font-size" : getElSize(40)
	});
	
	$("#tableDiv").css({
		//"height" : getElSize(1700),
		"width" : "90%"
	});
	
	$("#tableTd").css({
		"height" : getElSize(1700),
		"overflow" : "auto" 
	});
	
	$("#excel").css({
		"width" :getElSize(80),
		"position" : "absolute"
	});
	
	$("#excel").css({
		"left" :$("#sDate").offset().left - getElSize(100),
		"margin-top" : 0,
		"cursor" : "pointer"
	})
	
	$("#reportTable").css({
		"height" : originHeight - (marginHeight*2) -getElSize(50)
	});
	
	$("#part3_title").css({
		"height" : getElSize(100)
	});
	
	$(".container").css({
		//"background":"url('" +ctxPath + "/images/Background.png')",
		//"background-size": "100% 100%",
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$("#stateBorder").css({
		"position": "absolute",
		"width": contentWidth,
		"height" : contentHeight,
		"top" : marginHeight,
		"left" : marginWidth,
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$(".machine_icon, .init_machine_icon").css({
		"position" : "absolute",
//		"width" : contentWidth * 0.1,
//		"height" : contentWidth * 0.1,
		"cursor" : "pointer",
	});

	$(".circle, .init_circle").css({
		"background-color" : "white",
		"width" : contentWidth * 0.07,
		"height" : contentWidth * 0.07,
		"border-radius" : "50%",
	});
	
	// 페이지 위치 조정
//	for (var i = 2; i <= 5; i++) {
//		$("#part" + i).css({
//			"top" : $("#part" + (i - 1)).height() + originHeight
//		});
//	};

	$(".page").css({
		"top" : originHeight,
		"padding" : 0
	});
	
	$("#part1").css({
		"top" : 0
	});
	
	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(70),
	});

	$("#title_main").css({
		"font-size" : getElSize(100),
		"color" : "rgb(124,181,236)"
	});
	
	$("#part3_title").css({
		"padding" : getElSize(20),
	});

	$("#part3 .title").css({
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
	});
	
	$(".part3_no_padding").css({
		"padding" : 0,
	});
	
	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$("#barChart").css({
		"position" : "absolute",
		"bottom" : getElSize(20) +marginHeight,
		"left" : getElSize(30) + marginWidth,
		"border" : "1px solid white"
	});
	
	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(350),
		"font-weight" : "bolder"
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "black",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1,
		"margin-left" : getElSize(150)
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"width" : contentWidth * 0.04,
		"top" : marginHeight+getElSize(30),
		"left" : getElSize(30) + marginWidth,
		"z-index" : 5
	});
	
	$("#main_logo").css({
		"position" : "absolute",
		"width" : getElSize(600),
		"top" : $("#menu_btn").offset().top + getElSize(30), 
		"left" : $("#menu_btn").offset().left + $("#menu_btn").width() + getElSize(20),
		"display" : "inline",
		"z-index" : 1
	});
	

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 999999,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(60)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});


	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$(".wrap").css({
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		"margin" : getElSize(50)
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	var chart_height = getElSize(60);
	if(chart_height<20) chart_height = 20;
	
	$(".card_status").css({
		"height" : chart_height,
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.015
	});
	
	$(".menu_icon").css({
		"width" : getElSize(250),
		"height" : getElSize(250),
		"border-radius" : "50%",
	});

	$("p").css({
		"font-size" : getElSize(40),
		"font-weight" : "bolder"
	});
	
	$(".mainTable").not("#mainTable, #mainTable_block").css({
		"border-spacing" : getElSize(30)
	});
	
	$(".td_header").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"background-color" : "rgb(239,174,62)",
		"padding" : getElSize(20),
		"margin" : getElSize(10)
	});
	
	$("#reportTable .span").css({
		"color" : "black",
		"font-weight" : "bolder",
		"font-size" : getElSize(170)
	});
	
	$("#incycleTime_avg").css({
		"color" : "black"
	});
	
	$(".upDown").css({
		"width" : getElSize(100),
		"margin-top" : getElSize(100)
	});
	
	$("#upFont").css({
		"color" : "rgb(124,224,76)",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#downFont").css({
		"color" : "#FF3A3A",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#close_btn").css({
		"position" : "absolute",
		//"display" : "none",
		"width" : getElSize(100),
		"cursor" : "pointer",
		"left" : $("#machineListForTarget").offset().left + $("#machineListForTarget").width() +getElSize(50),
		"top" : $("#machineListForTarget").offset().top - getElSize(50),
 	});
	
	$("#thermometer, #thermometer2, #thermometer3").css({
		"width" : getElSize(80),
		"height" : getElSize(500),
		"margin-bottom" : getElSize(30),
		"margin-right" : getElSize(20),
		"margin-left" : getElSize(80),
		"border-radius" : getElSize(35),
		"margin- top" : getElSize(50),
		"float" : "left"
	});
	
	$("#thermometer .track, #thermometer2 .track, #thermometer3 .track").css({
		"width" : getElSize(30),
		"height" : getElSize(450),
		"top" : getElSize(25),
	});
	
	$("#thermometer .progress .amount, #thermometer2 .progress .amount, #thermometer3 .progress .amount").css({
		"padding": 0 + " " + getElSize(80) + " 0 " + getElSize(0),
		"font-size" : getElSize(30)
	});
	
	$("#diagram").css({
		"margin-top" : getElSize(50)
	});
	
	$("#reportDateDiv").css({
		"float" : "right",
		"right" : getElSize(50),
	});
	
	$("#sDate, #eDate").css({
//		"width" : getElSize(400),
//		"height" : getElSize(50),
		"font-size" : getElSize(30)
	});
	
	$("#map").css({
		"left" : (originWidth/2) - ($("#map").width()/2) - marginWidth 
	});
	
	$("#comName").css({
		"font-size" : getElSize(70)
	});
	
	$("#reportTable, #mainTable2").css("margin-top",$("#mainTable").offset().top);
	
	$("#alarm").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(60),
		"margin-top" : getElSize(30),
		"margin-left" : getElSize(50)
	});
	
	$(".upDownSpan").css({
		"font-size" : getElSize(300)
	});
	
	$("#statusBox").css({
		"position" : "absolute",
		"left" : getElSize(1500) + marginWidth,
		"bottom" : $("#barChart").height() + marginHeight + getElSize(30), 
		"width" : getElSize(1500)
	});
	
	$("#monthlyTarget").css({
		"position" : "absolute",
		"z-index" : 1,
		"color" : "rgb(104,206,19)",
		"font-weight" : "bolder",
		"top" : marginHeight + ($("#title_main").height()/2),
		"right" :  marginWidth + getElSize(500),
		"font-size" : getElSize(50)
	});
	
	$("#monthlyTargetBox").css({
		"position" : "absolute",
		"display" : "none"
	});
	
	$("#monthlyTargetBox").css({
		"top" : originHeight/2 - ($("#monthlyTargetBox").height()/2),
		"left" : originWidth/2 - ($("#monthlyTargetBox").width()/2),
		"display" : "none",
		"z-index" : 10,
		"padding" : getElSize(20),
		"background-color" : "green",
		//"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white"
	});
	
	$("#monthlyTargetValue").css({
		"font-size" :getElSize(50),
		"width" : getElSize(200),
	});
	
	$("#monthly_target_save_btn").css({
		"font-size" : getElSize(50),
		"background-color" : "white",
		"border-radius" : getElSize(10),
		"padding" :getElSize(10),
		"cursor" : "pointer"
	});
	
};

function drawStockChart() {
	var seriesOptions = [], seriesCounter = 0, names = [ 'MSFT', 'AAPL', 'GOOG' ],
	// create the chart when all data is loadedx
	createChart = function() {
		$('#container').highcharts('StockChart', {
			chart : {
				height : originHeight * 0.45
			},
			exporting : false,
			credits : false,
			rangeSelector : {
				selected : 4
			},

			rangeSelector : {
				buttons : [ {
					type : 'hour',
					count : 1,
					text : '1h'
				}, {
					type : 'day',
					count : 1,
					text : '1d'
				}, {
					type : 'month',
					count : 1,
					text : '1m'
				}, {
					type : 'year',
					count : 1,
					text : '1y'
				}, {
					type : 'all',
					text : 'All'
				} ],
				inputEnabled : true, // it supports only days
				selected : 4
			// all
			},

			yAxis : {
				labels : {
					formatter : function() {
						// return (this.value > 0 ? ' + ' : '') + this.value +
						// '%';
						return this.value
					}
				},
				plotLines : [ {
					value : 0,
					width : 2,
					color : 'silver'
				} ]
			},

			/*
			 * plotOptions: { series: { compare: 'percent' } },
			 */

			tooltip : {
				// pointFormat: '<span
				// style="color:{series.color}">{series.name}</span>:
				// <b>{point.y}</b> ({point.change}%)<br/>',
				valueDecimals : 2
			},

			series : seriesOptions
		});
	};

	seriesOptions[0] = {
		name : names[0],
		data : data_
	};

	/*
	 * $.each(names, function (i, name) {
	 * 
	 * $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' +
	 * name.toLowerCase() + '-c.json&callback=?', function (data) {
	 * seriesOptions[i] = { name: name, data: data_ };
	 * 
	 * seriesCounter += 1;
	 * 
	 * if (seriesCounter === names.length) { createChart(); } }); });
	 */

	Highcharts.createElement('link', {
		href : '//fonts.googleapis.com/css?family=Unica+One',
		rel : 'stylesheet',
		type : 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
		colors : [ "#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee",
				"#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF",
				"#aaeeee" ],
		chart : {
			backgroundColor : {
				linearGradient : {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 1
				},
				stops : [ [ 0, '#2a2a2b' ], [ 1, '#3e3e40' ] ]
			},
			style : {
				fontFamily : "'Unica One', sans-serif"
			},
			plotBorderColor : '#606063'
		},
		title : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase',
				fontSize : '20px'
			}
		},
		subtitle : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase'
			}
		},
		xAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			title : {
				style : {
					color : '#A0A0A3'

				}
			}
		},
		yAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			tickWidth : 1,
			title : {
				style : {
					color : '#A0A0A3'
				}
			}
		},
		tooltip : {
			backgroundColor : 'rgba(0, 0, 0, 0.85)',
			style : {
				color : '#F0F0F0'
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					color : '#B0B0B3'
				},
				marker : {
					lineColor : '#333'
				}
			},
			boxplot : {
				fillColor : '#505053'
			},
			candlestick : {
				lineColor : 'white'
			},
			errorbar : {
				color : 'white'
			}
		},
		legend : {
			itemStyle : {
				color : '#E0E0E3'
			},
			itemHoverStyle : {
				color : '#FFF'
			},
			itemHiddenStyle : {
				color : '#606063'
			}
		},
		credits : {
			style : {
				color : '#666'
			}
		},
		labels : {
			style : {
				color : '#707073'
			}
		},

		drilldown : {
			activeAxisLabelStyle : {
				color : '#F0F0F3'
			},
			activeDataLabelStyle : {
				color : '#F0F0F3'
			}
		},

		navigation : {
			buttonOptions : {
				symbolStroke : '#DDDDDD',
				theme : {
					fill : '#505053'
				}
			}
		},

		// scroll charts
		rangeSelector : {
			buttonTheme : {
				fill : '#505053',
				stroke : '#000000',
				style : {
					color : '#CCC'
				},
				states : {
					hover : {
						fill : '#707073',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					},
					select : {
						fill : '#000003',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					}
				}
			},
			inputBoxBorderColor : '#505053',
			inputStyle : {
				backgroundColor : '#333',
				color : 'silver'
			},
			labelStyle : {
				color : 'silver'
			}
		},

		navigator : {
			handles : {
				backgroundColor : '#666',
				borderColor : '#AAA'
			},
			outlineColor : '#CCC',
			maskFill : 'rgba(255,255,255,0.1)',
			series : {
				color : '#7798BF',
				lineColor : '#A6C7ED'
			},
			xAxis : {
				gridLineColor : '#505053'
			}
		},

		scrollbar : {
			barBackgroundColor : '#808083',
			barBorderColor : '#808083',
			buttonArrowColor : '#CCC',
			buttonBackgroundColor : '#606063',
			buttonBorderColor : '#606063',
			rifleColor : '#FFF',
			trackBackgroundColor : '#404043',
			trackBorderColor : '#404043'
		},

		// special colors for some of the
		legendBackgroundColor : 'rgba(0, 0, 0, 0.5)',
		background2 : '#505053',
		dataLabelsColor : '#B0B0B3',
		textColor : '#C0C0C0',
		contrastTextColor : '#F0F0F3',
		maskColor : 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	createChart();
};