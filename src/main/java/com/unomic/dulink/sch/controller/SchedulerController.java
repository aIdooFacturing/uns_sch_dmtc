package com.unomic.dulink.sch.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.ParamVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;
	
	//장비 요약 정보
	@Scheduled(fixedDelay = 10000)
	public void schAddDvcSmry(){
		System.out.println("Device Summary");
		if(CommonCode.isTest) return;
		String str, str2 = "";
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE),0,0,0);
		cal.set(Calendar.MILLISECOND, 0);
		Calendar cal2 = Calendar.getInstance();
		cal2.set(cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DATE),23,59,59);
		cal2.set(Calendar.MILLISECOND, 0);
		str = dayTime.format(cal.getTime());
		str2 = dayTime.format(cal2.getTime());
		
		ParamVo prmVo = new ParamVo();
		prmVo.setSDate(str);
		prmVo.setEDate(str2);
		
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		
		schService.addDvcSmry(prmVo);
	/*	prmVo.setDate(CommonFunction.getYstDay());
		schService.addDvcSmry(prmVo);*/
		
		LOGGER.error("run schAddDvcSmry.");
	}
	
	//타임차트 입력.
	@Scheduled(fixedDelay = 60000)
	public void schAddTmCht(){
		System.out.println("TimeChart Scheduel");
		if(CommonCode.isTest) return;
		ParamVo prmVo = new ParamVo();
		
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTime:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		System.out.println(dateTime);
		schService.addDvcTmChart(prmVo);
		LOGGER.error("run schAddTmCht.");
	}

	/*//장비 마지막 상태 업데이트
	@Scheduled(fixedDelay = 2000)
	public void schEditDvcLast(){
		if(CommonCode.isTest) return;
		schService.editDvcLastStatus();
		schService.editDvcLast();
		ParamVo prmVo = new ParamVo();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		schService.setNoCon(prmVo);
		LOGGER.error("run schEditDvcLast.");
	}*/
	
	
	
	//초/분/시/ 00 한국시간보다 9시간빨라야함.
	@Scheduled(fixedRate=180000)
	public void schAddNightStatus(){
		LOGGER.error("run schAddNightStatus.");
		if(CommonCode.isTest) return;
		ParamVo prmVo = new ParamVo();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setTgDate(strDate);
		schService.addNightStatus(prmVo);
		LOGGER.error("run schAddNightStatus.");
	}
	
	//초/분/시/ 00 한국시간보다 9시간빨라야함.
	@Scheduled(cron="0 0 16 * * ?")
	public void schBkOldAdtStatus(){
		LOGGER.error("start run schBkOldAdtStatus.");
		if(CommonCode.isTest) return;
		schService.bkOldAdtStatus();
		
		LOGGER.error("end run schBkOldAdtStatus.");
	}
	
	
	
	//?tgDate=yyyy-mm-dd
	@RequestMapping(value="runAddNightStatus")
	@ResponseBody
	public String runAddNightStatus(String tgDate){
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setTgDate(tgDate);
		schService.addNightStatus(prmVo);
		LOGGER.error("run addNightStatus.");
		return "OK";
	}
		
	@RequestMapping(value="runEditDvcLast")
	@ResponseBody
	public String runEditDvcLast(String tgDate){
		schService.editDvcLastStatus();
		schService.editDvcLast();
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(tgDate);
		schService.setNoCon(prmVo);
		LOGGER.error("run schEditDvcLast.");
		return "OK";
	}
	
	

	@RequestMapping(value="noConTest")
	@ResponseBody
	public String noConTest(){
		ParamVo prmVo = new ParamVo();
		
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		schService.setNoCon(prmVo);
		return "OK:"+strDate;
	}
	
	@RequestMapping(value="spTest")
	@ResponseBody
	public void spTest(){
		//DeviceVo dvcVo = new DeviceVo();
		//dvcVo.setWorkDate("2015-10-06");
		//deviceService.calcDeviceTimesTest(dvcVo);
		//deviceService.calcDeviceTimes(dvcVo);
		//calcDeviceTimes();
	}
	
	//?date=2017-06-23
	@RequestMapping(value="addDvcSmryTest")
	@ResponseBody
	public String addDvcSmryTest(ParamVo prmVo){
		//prmVo.setDate("2017-06-23");
		schService.addDvcSmry(prmVo);
		
		return "OK";
	}
	
	@RequestMapping(value="addDvcLastTest")
	@ResponseBody
	public String addDvcLastTest(){
		schService.addDvcLast();
		return "OK";
	}
	
	
	@RequestMapping(value="editDvcLastTest")
	@ResponseBody
	public String editDvcLastTest(){
		schService.editDvcLast();
		return "OK";
	}
	
	@RequestMapping(value="editDvcLastStatusTest")
	@ResponseBody
	public String editDvcLastStatusTest(){
		schService.editDvcLastStatus();
		return "OK";
	}
	
	@RequestMapping(value="addTmChtTest")
	@ResponseBody
	public String addTmChtTest(){
		
		ParamVo prmVo = new ParamVo();
		
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTime:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		
		schService.addDvcTmChart(prmVo);
		LOGGER.error("run schAddTmCht.");
		
		return "OK:"+CommonFunction.getTodayDateTime();
		
	}
	
	@RequestMapping(value="batchAddDvcSmry")
	@ResponseBody
	public String batchTimeChart(ParamVo inputVo){
		LOGGER.info("stDate:"+inputVo.getStDate());
		LOGGER.info("edDate:"+inputVo.getEdDate());
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		LOGGER.info("inputVo.getStDate():"+inputVo.getStDate());
		LOGGER.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
			ParamVo prmVo = new ParamVo();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	       //DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.zzz");
	       dateFormat.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
           Date dateSt = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());
           LOGGER.info("tgDate:"+tgDate);

           inputVo.setTgDate(tgDate);
           
           LOGGER.info("getTgDate:"+inputVo.getTgDate());
           LOGGER.info("edDate:"+edDate);
           

           //CommonFunction.dateTime2Mil(tgDate)
           //while(!tgDate.equals(edDate)){
           //while(CommonFunction.dateTime2Mil(tgDate) < (CommonFunction.dateTime2Mil(edDate))){
           while(!tgDate.equals(edDate)){
        	   LOGGER.info("@@@@@@@@@@@@@@@@@@ING@@@@@@@@@@@@@@@@@@");
        	   //deviceService.addTimeChart(inputVo);
        	   prmVo.setDate(inputVo.getTgDate());
        	   
        	   schService.addDvcSmry(prmVo);

        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTgDate(tgDate);
               LOGGER.info("tgDate:"+tgDate);
           }
           // EOL
           LOGGER.info("@@@@@@@@@@@@@@@@@@EOL@@@@@@@@@@@@@@@@@@");
           LOGGER.info("edDate:"+tgDate);
           LOGGER.info("edDate:"+edDate);
 
		return "OK";
	}
	
}

